<?php

/* If $debug is true, the pre-processing functions below will be executed. This
 * results in the playlist.min.xml being created which is used by the ximpel app.
 * This only has to be done one time when the app is done, so after that time it
 * can be set to false */
$debug = false;

if ($debug) {
    /* Setup debug environment */
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

function debug_print($string, $do_htmlentities) {
    if ($do_htmlentities)
        printf("<pre>%s</pre>",print_r(htmlentities($string), true));
    else
        printf("<pre>%s</pre>",print_r($string, true));
}

/* Arrow configuration */
$arrow_default_xml = "<overlay width='128px' height='128px' backgroundColor='' alpha='0.75' hoverAlpha='1' ";
$arrow_sizes = ["up" => [896, 90], "right" => [1702, 476], "down" => [896, 862], "left" => [90, 476]];

if ($debug) {
    $xml_string = file_get_contents("./playlist.xml");
    $xml_string = resolve_xml_imports($xml_string);
    $xml_string = add_level_arrows($xml_string, $arrow_default_xml, $arrow_sizes);
    $xml_string = remove_xml_whitespace($xml_string);

    $playlist_file = fopen("playlist.min.xml", "w") or die("Unable to open file");

    fwrite($playlist_file, $xml_string);
    fclose($playlist_file);
}

function resolve_xml_imports($xml_string) {
    preg_match_all("/@import .* /", $xml_string, $matches);
    $matches = $matches[0];

    for ($i = 0; $i < count($matches); $i++) {
        $matches[$i] = explode(" ", $matches[$i])[1];

        $xml_import_string = file_get_contents($matches[$i]);
        $xml_string = preg_replace("/<!-- @import .* -->/", $xml_import_string, $xml_string, 1);
    }

    return $xml_string;
}

function add_level_arrows($xml_string, $arrow_default_xml, $arrow_sizes) {
    preg_match_all("/<level.*>/", $xml_string, $levels);
    $levels = $levels[0];

    /* Loop over each <level> tag, and add overlays with an arrow as background
     * image and a leadsTo attribute to the next level to them. Also change
     * them to the <image> tag (also </level> -> </image>) */
    for ($i = 0; $i < count($levels); $i++) {
        $levels[$i] = trim($levels[$i], "\<\>");
        $split_level = explode(" ", $levels[$i]);

        $new_xml = "<image " . $split_level[1] . ">";

        for ($j = 2; $j < count($split_level); $j++) {
            $split_overlay = explode("=", $split_level[$j]);
            $direction = $split_overlay[0];
            $leads_to_level = trim($split_overlay[1], '"');
            $x = $arrow_sizes[$direction][0];
            $y = $arrow_sizes[$direction][1];

            $new_xml .= $arrow_default_xml . "image='media/arrow-" . $direction .
                        ".png' leadsTo='" . $leads_to_level . "' x='" . $x .
                        "px' y='" . $y . "px' />";

        }

        $xml_string = preg_replace("/<level.*>/", $new_xml, $xml_string, 1);
        $xml_string = preg_replace("/<\/level>/", "</image>", $xml_string, 1);
    }
    return $xml_string;
}

function remove_xml_whitespace($xml_string) {
    $xml_string = preg_replace("/\>\s+\</", "><", $xml_string);
    return preg_replace("/\n*/", "", $xml_string);
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Bio Detective</title>
        <script src="//code.jquery.com/jquery-1.12.3.min.js"></script>
        <link rel="stylesheet" href="ximpel/ximpel.css" type="text/css" />
        <script type="text/javascript" src="ximpel/ximpel.min.js"></script>
        <script type="text/javascript" src="ximpel/ximpel-extend.js"></script>

        <script type="text/javascript">
            $( document ).ready(function(){
                // Define a XimpelApp object by passing the following arguments:
                // - The appId which is just a unique name for this presentation.
                // - The path to the playlist file relative to this html document.
                // - The path to the config file relative to this html document
                // - An object containing multiple optional settings such a the
                //   the width/height of the app and the element to use.
                var myApp = new ximpel.XimpelApp(
                    'bio-detective',
                    'playlist.min.xml',
                    'config.xml',
                    {'appElement': 'ximpel',
                     'appWidth': '640px',
                     'appHeight': '360px'
                    }
                );

                // After creating a XimpelApp object tell the object to load the given playlist and config file.
                myApp.load( {
                    'autoPlay': true
                }).done( function(){
                    // This function is executed once the ximpel app has finished loading the playlist
                    // and config file and has been fully initialized (ie its ready to play or it is
                    // already playing if autoplay was set to true). This function can be used to interact
                    // with the ximpel app using external javascript.
                    var player = myApp.ximpelPlayer;

                    // Init score display
                    var showScore = player.getConfigProperty('showScore');

                    if (showScore){
                        $('.scoreContainer').show();
                        $('#score').text(player.getVariable("score"));
                        // Add more score initializations here

                        player.addEventHandler( 'variable_updated', function( variableId ){
                            $('#' + variableId).text( player.getVariable( variableId ) );
                        });
                    }
                    else {
                        $('.scoreContainer').hide();
                    }
                }.bind(this));
            });
        </script>
    </head>

    <body>
        <div id="ximpel">
            <div class="scoreContainer">
                <div class="score">Score: <span id="score"></span></div>
                <!-- Add more score containers here -->
            </div>
        </div>
    </body>
</html>

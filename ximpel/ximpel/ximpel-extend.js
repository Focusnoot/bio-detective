/* Extension: Add a conditionModel to the overlay */
ximpel.OverlayModel.prototype.conditionModel = null;

/* Extend processOverlayNode function to support a condition attribute */
ximpel.Parser.prototype.processOverlayNode = function( playlistModel, domElement, index ){
	// Get some info about the current domElement (like its parent, its children, etc)
	var info = this.getDomElementInfo( domElement );
	var overlayModel = new ximpel.OverlayModel();

	// Store index for stable sorting
	overlayModel.index = index;

	// Process and store the attributes of the <overlay> element.
	for( var i=0; i<info.nrOfAttributes; i++ ){
		var attributeName = info.attributes[i].name;
		var attributeValue = info.attributes[i].value;
		if( attributeName === 'x' ){
			overlayModel.x = parseInt(attributeValue);
		} else if( attributeName === 'y' ){
			overlayModel.y = parseInt( attributeValue);
		} else if( attributeName === 'shape' ){
			overlayModel.shape = attributeValue;
		} else if( attributeName === 'width' ){
			overlayModel.width = parseInt(attributeValue);
		} else if( attributeName === 'height' ){
			overlayModel.height = parseInt(attributeValue);
		} else if( attributeName === 'side' ){
			overlayModel.side = parseInt(attributeValue);
		} else if( attributeName === 'diameter' ){
			overlayModel.diameter = parseInt(attributeValue);
		} else if( attributeName === 'leadsTo' ){
			var leadsToModel = new ximpel.LeadsToModel();
			leadsToModel.subject = attributeValue;
			overlayModel.leadsToList.push( leadsToModel );
		} else if( attributeName === 'startTime' ){
			overlayModel.startTime = parseFloat(attributeValue)*1000;
		} else if( attributeName === 'duration' ){
			overlayModel.duration = parseFloat(attributeValue)*1000;
		} else if( attributeName === 'text' ){
			overlayModel.text = attributeValue;
		} else if( attributeName === 'alpha' ){
			overlayModel.opacity = attributeValue;
		} else if( attributeName === 'hoverAlpha' ){
			overlayModel.hoverOpacity = attributeValue;
		} else if( attributeName === 'backgroundColor' ){
			overlayModel.backgroundColor = attributeValue;
		} else if( attributeName === 'hoverBackgroundColor' ){
			overlayModel.hoverBackgroundColor = attributeValue;
		} else if( attributeName === 'textColor' ){
			overlayModel.textColor = attributeValue;
		} else if( attributeName === 'hoverTextColor' ){
			overlayModel.hoverTextColor = attributeValue;
		} else if( attributeName === 'fontFamily' ){
			overlayModel.fontFamily = attributeValue;
		} else if( attributeName === 'hoverFontFamily' ){
			overlayModel.hoverFontFamily = attributeValue;
		} else if( attributeName === 'fontSize' ){
			overlayModel.fontSize = attributeValue;
		} else if( attributeName === 'hoverFontSize' ){
			overlayModel.hoverFontSize = attributeValue;
		} else if( attributeName === 'image' ){
			overlayModel.backgroundImage = attributeValue;
		} else if( attributeName === 'hoverImage' ){
			overlayModel.hoverBackgroundImage = attributeValue;
		} else if( attributeName === 'waitForMediaComplete' ){
			overlayModel.waitForMediaComplete = attributeValue;
		} else if( attributeName === 'description' ){
			overlayModel.description = attributeValue;
		/* Extension: Add support for the condition attribute */
		} else if( attributeName === 'condition' && attributeValue){
			overlayModel.conditionModel = new ximpel.ConditionModel();
			overlayModel.conditionModel.condition = attributeValue;
		} else{
			ximpel.warn('Parser.processOverlayNode(): Invalid attribute ignored! Attribute \''+attributeName+'\' on element <'+info.tagName+'> is not supported. Make sure you spelled the attribute name correctly.');
		}
	}

	// Process and store the child elements of the <overlay> element.
	for( var i=0; i<info.nrOfChildElements; i++ ){
		var child = info.childElements[i];
		var childName = child.nodeName;

		if( childName === 'score' || childName === 'variable' ){
			var variableModifier = this.processVariableNode( playlistModel, child );
			overlayModel.variableModifiers.push( variableModifier );
		} else if( childName === 'leadsTo' ){
			var leadsToModel = this.processLeadsToNode( playlistModel, child );
			overlayModel.leadsToList.push( leadsToModel );
		} else{
			ximpel.warn('Parser.processOverlayNode(): Invalid child ignored! Element <'+info.tagName+'> has child <'+childName+'>. Allowed children: ' + this.validChildren[info.tagName].toString()  + '.');
		}
	}
	return overlayModel;
}

/* Extend the update overlay function to be able to conditionally show some overlays */
ximpel.MediaPlayer.prototype.updateOverlays = function( currentPlayTime ){
	// Check if there are any overlays which need to be hidden/stopped by iterating over all the currently playing overlays.
	for( var i=0; i<this.playingOverlays.length; i++ ){
		var overlayEndTime = this.playingOverlays[i].endTime;

		// Check if the current play time is ahead of the overlay's end time...
		if( currentPlayTime >= overlayEndTime && overlayEndTime !== 0 ){
			// The end time of the overlay wa reached so we destroy the overlay view.
			this.playingOverlays[i].view.destroy();

			// Remove the overlay from the array of playing overlays.
			this.playingOverlays.splice( i, 1 );

			i--; // Since we deleted an overlay i should be decreased by 1 to not disturb our for loop.
		}
	}

	// Check if an overlay needs to be displayed.
	var nrOfOverlaysToCheck = this.overlaysSortedByStartTime.length;
	for( var i=this.overlayIndexToStartNext; i<nrOfOverlaysToCheck; i++ ){
		var overlayModel = this.overlaysSortedByStartTime[i];

		/* Extension: Add a check to see if the condition of the overlay is false, if it is don't show the overlay */
		if (overlayModel.conditionModel && !this.player.evaluateCondition(overlayModel.conditionModel)) {
			continue;
		}
		else if( overlayModel.startTime > currentPlayTime || currentPlayTime === 0 ){
			// The overlay is not ready to be played yet. Since the overlays are sorted on startTime
			// we know that the rest of the overlays are not ready either so we break out of the for loop.
			break;
		} else{
			// Its time to show this overlay, so we create its view and attach a click handler to it.
			var overlayView = new ximpel.OverlayView( overlayModel );
			overlayView.render( this.$playerElement );
			overlayView.onOneClick( this.handleOverlayClick.bind(this, overlayModel, overlayView ) );

			// Add the view to an array containing the currently playing overlays.
			var overlayEndTime = overlayModel.duration > 0 ? overlayModel.startTime+overlayModel.duration : 0; // an end time of 0 means until the end of the media item.
			this.playingOverlays.push( {'view': overlayView, 'model': overlayModel, 'endTime': overlayEndTime} );
			// An overlay has now been displayed, so we make overlayIndexToStartNext point to the next overlay which is the one to be displayed next.
			this.overlayIndexToStartNext++;
		}
	}
}
